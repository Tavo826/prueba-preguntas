# TODO:
# 

# FIXME:
# Comentar linea que borra la tabla de jugadores


import sqlite3
from init_DB import initialize_DB


class DataBase:

    def __init__(self):
        self.db_connection = sqlite3.connect('database.db')

    def get_connection(self):
        self.db_connection.row_factory = sqlite3.Row
        return self.db_connection

    def create(self):
        with open('squema.sql') as db:
            self.db_connection.executescript(db.read())

    def initialize(self):
        conn = self.get_connection()
        initialize_DB(conn)
        
        conn.commit()
        conn.close()

    def create_and_initialize(self):
        self.create()
        self.initialize()    



class Player:
    def __init__(self, id_document, name, last_name):
        self.name = name
        self.lastName = last_name
        self.__document = id_document

    def __str__(self):
        return "{} {}".format(self.name, self.lastName)

    def save(self):
        database = DataBase()
        conn = database.get_connection()

        conn.execute('INSERT INTO info_players (player_document, player_name, player_last_name) VALUES (?,?,?)', (self.__document, self.name, self.lastName))

        conn.commit()
        conn.close()

    def add_score(self, score):
        pass


class Round:
    def __init__(self, category):
        self.category = category
        categories = {
            'easy': 'easy_questions',
            'normal': 'normal_questions',
            'medium': 'medium_questions',
            'hard': 'hard_questions',
            'extreme': 'extreme_questions',
        }

    def ask_question():
        database = DataBase()
        conn = database.get_connection()       

        questions = conn.execute('SELECT * from ')

        conn.commit()
        conn.close()


class Game:

    def show_instructions(self):
        print('''
        El juego consiste en contestar una serie de 5 preguntas, cada una con 4 opciones de respuesta. Si contesta correctamente a cada pregunta, sumará cada vez más dinero a sus bolsillos, pero si la respuesta es incorrecta saldrá del juego sin el dinero que ha acumulado.

        Puede elegir retirarse antes de responder cada pregunta y llevarse así el acumulado actual.

        Cada categoría cuenta con los siguientes premios:\n

        Categoría 1 -> $100.000\n
        Categoría 1 -> $500.000\n
        Categoría 1 -> $1.000.000\n
        Categoría 1 -> $5.000.000\n
        Categoría 1 -> $10.000.000
        ''')

    
    def play(self):
        # print('Nuevo juego'.center(30, '-'))
        # instructions = int(input('\nPara mostrar las instrucciones ingrese "1", para empezar el juego ingrese "0": '))
        # if instructions:
        #     self.show_instructions()

        name = input('\nIgrese su nombre: ')
        last_name = input('Ingrese su apellido: ')
        document = input('Ingrese su documento: ')

        player = Player(document, name, last_name)
        player.save()
        


def main():
    dataBase = DataBase()
    dataBase.create_and_initialize()

    game = Game()
    game.play()

    dataBase.db_connection.close()


if __name__ == '__main__':
    main()

