import sqlite3

def initialize_DB(cur):

    # Category 1 questions

    questions_1 = [
        (1, 'El borrador de un lápiz suele estar hecho de'),
        (2, '¿Cuántos dados se necesitan para jugar parqués?'),
        (3, '¿Cuál de los siguientes animales es semiacuático?'),
        (4, '¿Cuál de estas frutas debe necesariamente ser pelada antes de ser consumida?'),
        (5, 'Romperle la cintura a un futbolista quiere decir'),
    ]

    cur.executemany('INSERT INTO easy_questions VALUES(?,?)',  questions_1);

    # Category 1 answers

    answers_1 = [
        (1, 1, 'Arcilla', 0),
        (2, 1, 'Goma', 1),
        (3, 1, 'Metal', 0),
        (4, 1, 'Arcilla', 0),

        (5, 2, 'Uno', 0),
        (6, 2, 'Dos', 1),
        (7, 2, 'Tres', 0),
        (8, 2, 'Cuatro', 0),

        (9, 3, 'El caballo', 0),
        (10, 3, 'El águila', 0),
        (11, 3, 'La nutria', 1),
        (12, 3, 'El koala', 0),

        (13, 4, 'La pera', 0),
        (14, 4, 'La manzana', 0),
        (15, 4, 'La uva', 0),
        (16, 4, 'La piña', 1),

        (17, 5, 'Enviarlo a la enfermería', 0),
        (18, 5, 'Evitarlo con un amague', 1),
        (19, 5, 'No darle ni una gota de agua', 0),
        (20, 5, 'Dejarlo sentado en la banca', 0),
    ]

    cur.executemany('INSERT INTO easy_answers VALUES(?,?,?,?)', answers_1);


    # Category 2 questions

    questions_2 = [
        (1, '¿Cuál es el promedio de 3, 4, 5 y 6?'),
        (2, 'Las frutas que funcionan como exfoliantes son usadas en tratamientos'),
        (3, 'En el titular de un peródico debe estar'),
        (4, "¿Cuál era la particularidad de Charlie, el de la serie de televisión 'Los Ángeles de Charlie'?"),
        (5, '¿De qué color se ve la Estatua de la Libertad actualmente?'),
    ]

    cur.executemany('INSERT INTO normal_questions VALUES(?,?)', questions_2);

    # Category 2 answers

    answers_2 = [
        (1, 1, '4', 0),
        (2, 1, '4.5', 1),
        (3, 1, '5', 0),
        (4, 1, '5.5', 0),

        (5, 2, 'Cosméticos', 1),
        (6, 2, 'Hepáticos', 0),
        (7, 2, 'Sicológicos', 0),
        (8, 2, 'Renales', 0),

        (9, 3, 'La síntesis de la información', 1),
        (10, 3, 'La firma del periodista investigador', 0),
        (11, 3, 'Las recomendaciones para el lector', 0),
        (12, 3, 'La fuentes plenamente identificadas', 0),

        (13, 4, 'Que era muy alto', 0),
        (14, 4, 'Que nunca apareció en la serie', 1),
        (15, 4, 'Que llevaba monóculo', 0),
        (16, 4, 'Que caminaba con un bastón', 0),

        (17, 5, 'Blanco', 0),
        (18, 5, 'Negro', 0),
        (19, 5, 'Verde azuloso', 1),
        (20, 5, 'Violeta', 0),
    ]

    cur.executemany('INSERT INTO normal_answers VALUES(?,?,?,?)', answers_2);


    # Category 3 questions

    questions_3 = [
        (1, 'Las técincas de grabado e impresión pertenecen a las artes'),
        (2, '¿Cómo se le dice a algo que se da genuinamente?'),
        (3, 'Una de las primeras decisiones de Barack Obama en la presidencia fue'),
        (4, 'La geomorfología es la ciencia que estudia'),
        (5, '¿Cuántos jugadores se necesitan para una partida de bridge?'),
    ]

    cur.executemany('INSERT INTO medium_questions VALUES(?,?)', questions_3);

    # Category 3 answers

    answers_3 = [
        (1, 1, 'Marciales', 0),
        (2, 1, 'Escénicas', 0),
        (3, 1, 'Musicales', 0),
        (4, 1, 'Gráficas', 1),

        (5, 2, 'Formalismo', 0),
        (6, 2, 'Dádiva', 1),
        (7, 2, 'Gesto', 0),
        (8, 2, 'Lagarteo', 0),

        (9, 3, 'Pintar la alcoba de rosado', 0),
        (10, 3, 'Cambiar su avión por un Airbus', 0),
        (11, 3, 'Cerrar la cárcel de Guantánamo', 1),
        (12, 3, 'Nombrar a Bush embajador en Marte', 0),

        (13, 4, 'Las gemas', 0),
        (14, 4, 'La morfología humana', 0),
        (15, 4, 'La corteza terrestre', 1),
        (16, 4, 'Los insectos', 0),

        (17, 5, '2 jugadores', 0),
        (18, 5, '4 jugadores', 1),
        (19, 5, '6 jugadores', 0),
        (20, 5, '8 jugadores', 0),
    ]

    cur.executemany('INSERT INTO medium_answers VALUES(?,?,?,?)', answers_3);


    # Category 4 questions

    questions_4 = [
        (1, "'Flotar como una mariposa, picar como una abeja' era la frase de combate de"),
        (2, 'Edgar Rice Burroughs es el creador de'),
        (3, '¿Quién fue el fundador del budismo?'),
        (4, 'La floritura es un término principalmente'),
        (5, 'Antes del peso, la moneda colombiana era el'),
    ]

    cur.executemany('INSERT INTO hard_questions VALUES(?,?)', questions_4);

    # Category 4 answers

    answers_4 = [
        (1, 1, 'Rocky Valdés', 0),
        (2, 1, 'Carlos Monzón', 0),
        (3, 1, 'Kid Pambelé', 0),
        (4, 1, 'Muhammad Alí', 1),

        (5, 2, 'Batman', 0),
        (6, 2, 'Tarzán', 1),
        (7, 2, 'Supermán', 0),
        (8, 2, 'El Hombre Araña', 0),

        (9, 3, 'Mohama', 0),
        (10, 3, 'Siddhartha', 1),
        (11, 3, 'Vishnu', 0),
        (12, 3, 'Krisgna', 0),

        (13, 4, 'Musical', 1),
        (14, 4, 'Político', 0),
        (15, 4, 'Deportivo', 0),
        (16, 4, 'Taurino', 0),

        (17, 5, 'Talero', 0),
        (18, 5, 'Florín', 0),
        (19, 5, 'Colombiano', 0),
        (20, 5, 'Real', 1),
    ]

    cur.executemany('INSERT INTO hard_answers VALUES(?,?,?,?)', answers_4);


    # Category 5 questions

    questions_5 = [
        (1, 'Los sucesores de Alejandro Magno en Egipto conformaron la dinastía'),
        (2, 'Ludwig van Beethoven se casó'),
        (3, 'Europa y Asia se separan por'),
        (4, '¿Cuál es el nombre de la raza de perro comúnmente conocida como perro salchicha?'),
        (5, 'En la mitología escandinava los enanos están relacionados con'),
    ]

    cur.executemany('INSERT INTO extreme_questions VALUES(?,?)', questions_5);

    # Category 5 answers

    answers_5 = [
        (1, 1, 'Sarracena', 0),
        (2, 1, 'Filípica', 0),
        (3, 1, 'Ptolemaica', 1),
        (4, 1, 'Alejandrina', 0),

        (5, 2, 'Una vez', 0),
        (6, 2, 'Dos veces', 0),
        (7, 2, 'Tres veces', 0),
        (8, 2, 'Nunca', 1),

        (9, 3, 'Mar de Japón', 0),
        (10, 3, 'Península de Kamchatka', 0),
        (11, 3, 'Montes Urales', 1),
        (12, 3, 'Isla de Borneo', 0),

        (13, 4, 'Yorkshire', 0),
        (14, 4, 'Dachshund', 1),
        (15, 4, 'Caniche', 0),
        (16, 4, 'Galgo', 0),

        (17, 5, 'Los árboles y frutos', 0),
        (18, 5, 'Los lagos y playas', 0),
        (19, 5, 'El ciclo del agua', 0),
        (20, 5, 'Las piedras y los subterráneos', 1),
    ]

    cur.executemany('INSERT INTO extreme_answers VALUES(?,?,?,?)', answers_5);