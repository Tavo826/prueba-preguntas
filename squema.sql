DROP TABLE IF EXISTS easy_questions;
DROP TABLE IF EXISTS normal_questions;
DROP TABLE IF EXISTS medium_questions;
DROP TABLE IF EXISTS hard_questions;
DROP TABLE IF EXISTS extreme_questions;
DROP TABLE IF EXISTS easy_answers;
DROP TABLE IF EXISTS normal_answers;
DROP TABLE IF EXISTS medium_answers;
DROP TABLE IF EXISTS hard_answers;
DROP TABLE IF EXISTS extreme_answers;
-- Comentar
DROP TABLE IF EXISTS info_players;

CREATE TABLE easy_questions(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    question TEXT NOT NULL
);

CREATE TABLE normal_questions(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    question TEXT NOT NULL
);

CREATE TABLE medium_questions(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    question TEXT NOT NULL
);

CREATE TABLE hard_questions(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    question TEXT NOT NULL
);

CREATE TABLE extreme_questions(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    question TEXT NOT NULL
);

CREATE TABLE easy_answers(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    id_question INTEGER NOT NULL,
    answer TEXT NOT NULL,
    correct INTEGER DEFAULT 0
);

CREATE TABLE normal_answers(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    id_question INTEGER NOT NULL,
    answer TEXT NOT NULL,
    correct INTEGER DEFAULT 0
);

CREATE TABLE medium_answers(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    id_question INTEGER NOT NULL,
    answer TEXT NOT NULL,
    correct INTEGER DEFAULT 0
);

CREATE TABLE hard_answers(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    id_question INTEGER NOT NULL,
    answer TEXT NOT NULL,
    correct INTEGER DEFAULT 0
);

CREATE TABLE extreme_answers(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    id_question INTEGER NOT NULL,
    answer TEXT NOT NULL,
    correct INTEGER DEFAULT 0
);

CREATE TABLE info_players(
    player_document INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    player_name TEXT NOT NULL,
    player_last_name TEXT NOT NULL,
    player_score INTEGER
);